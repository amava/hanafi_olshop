<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function _view($view,$data=''){
		$this->load->view('core/header',$data);
		$this->load->view($view,$data);
		if(!isset($data['admin'])){
			$this->load->view('core/footer');
		}
		$this->load->view('core/init');
		if(isset($data['pagescript'])){
			$this->load->view($data['pagescript']);
		}
	}

	public function index()
	{
		$data['pagescript'] = 'script/index';
		$this->_view('index',$data);
	}

	public function product(){
		$data['pagescript'] = 'script/index';
		$this->_view('product',$data);
	}

	public function pembayaran(){
		$data['pagescript'] = 'script/index';
		$this->_view('pembayaran',$data);
	}

	public function penerima(){
		$data['pagescript'] = 'script/index';
		$this->_view('penerima',$data);
	}

	public function checkout(){
		$data['pagescript'] = 'script/index';
		$this->_view('checkout',$data);
	}

	public function detail(){
		$data['pagescript'] = 'script/detail';
		$this->_view('product_detail',$data);
	}

	public function status(){
		$data['pagescript'] = 'script/index';
		$this->_view('status',$data);
	}

	public function admin(){
		$data['admin'] = 1;
		$data['auth'] = 1;
		$this->_view('admin',$data);
	}

	public function daftar_produk(){
		$data['admin'] = 1;
		$data['auth'] = 1;
		$this->_view('daftar_produk',$data);
	}

	public function banner(){
		$data['admin'] = 1;
		$data['auth'] = 1;
		$this->_view('banner',$data);
	}

	public function promo(){
		$data['admin'] = 1;
		$data['auth'] = 1;
		$this->_view('promo',$data);
	}

	public function transaksi(){
		$data['admin'] = 1;
		$data['auth'] = 1;
		$this->_view('transaksi',$data);
	}

	public function diskon(){
		$data['admin'] = 1;
		$data['auth'] = 1;
		$this->_view('diskon',$data);
	}

	public function users(){
		$data['admin'] = 1;
		$data['auth'] = 1;
		$this->_view('users',$data);
	}

	public function tambah_user(){
		$data['admin'] = 1;
		$data['auth'] = 1;
		$this->_view('tambah_user',$data);
	}

	public function login(){
		$this->load->view('login_admin');
	}
}
