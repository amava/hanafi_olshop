<div class="container">
	<div class="row">
		<div class="col-12">
			<div id="news" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
				    <li data-target="#news" data-slide-to="0" class="active"></li>
				    <li data-target="#news" data-slide-to="1"></li>
				    <li data-target="#news" data-slide-to="2"></li>
				 </ol>
			  <div class="carousel-inner">
			    <div class="carousel-item active">
			      <img class="d-block w-100" src="<?=base_url('asset/images/bg.jpg')?>" alt="First slide">
			      <div class="carousel-caption d-none d-md-block">
				    <h3>First Slide<h3>
				    <p>Captions</p>
				  </div>
			    </div>
			    <div class="carousel-item">
			      <img class="d-block w-100" src="<?=base_url('asset/images/bg2.jpg')?>" alt="Second slide">
			      <div class="carousel-caption d-none d-md-block">
				    <h3>2nd Slide<h3>
				    <p>Captions</p>
				  </div>
			    </div>
			    <div class="carousel-item">
			      <img class="d-block w-100" src="<?=base_url('asset/images/bg3.jpg')?>" alt="Third slide">
			      <div class="carousel-caption d-none d-md-block">
				    <h3>3rd Slide<h3>
				    <p>Captions</p>
				  </div>
			    </div>
			  </div>
			  <a class="carousel-control-prev" href="#news" role="button" data-slide="prev">
			    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="carousel-control-next" href="#news" role="button" data-slide="next">
			    <span class="carousel-control-next-icon" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-6 promo-holder">
			<div class="glide">
			    <div class="glide__wrapper">
			        <ul class="glide__track">
			            <li class="glide__slide"><img src="<?=base_url('asset/images/cbr250rr.jpg')?>"></li>
			            <li class="glide__slide"><img src="<?=base_url('asset/images/r1m.jpg')?>"></li>
			            <li class="glide__slide"><img src="<?=base_url('asset/images/ninja.jpg')?>"></li>
			        </ul>
			    </div>
			    <div class="glide__bullets"></div>
			</div>
		</div>
		<div class="col-6">
			<div class="row">
				<div class="col-12 new-holder">
					<div id="glide" class="glide">
					    <div class="glide__arrows">
					        <button class="glide__arrow prev" data-glide-dir="<">prev</button>
					        <button class="glide__arrow next" data-glide-dir=">">next</button>
					    </div>
					    <div class="glide__wrapper">
					        <ul class="glide__track">
					            <li class="glide__slide"><img src="<?=base_url('asset/images/mini1.jpg')?>"></li>
					            <li class="glide__slide"><img src="<?=base_url('asset/images/mini2.jpg')?>"></li>
					            <li class="glide__slide"><img src="<?=base_url('asset/images/mini3.jpg')?>"></li>
					        </ul>
					    </div>
					    <div class="glide__bullets"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 disc-holder">
					<div class="glide">
					    <div class="glide__wrapper">
					        <ul class="glide__track">
					            <li class="glide__slide"><img src="<?=base_url('asset/images/mini3.jpg')?>"></li>
					            <li class="glide__slide"><img src="<?=base_url('asset/images/mini1.jpg')?>"></li>
					            <li class="glide__slide"><img src="<?=base_url('asset/images/mini2.jpg')?>"></li>
					        </ul>
					    </div>
					    <div class="glide__bullets"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row section">
		<h4 class="title-line">Rekomendasi</h4>
	</div>
	<div class="row offers">
		<div class="col product loved">
			<img src="<?=base_url('asset/images/r1m.jpg')?>">
			<span class="caption">Caption</span>
		</div>
		<div class="col product">
			<img src="<?=base_url('asset/images/r1m.jpg')?>">
			<span class="caption">Caption</span>
		</div>
		<div class="col product">
			<img src="<?=base_url('asset/images/r1m.jpg')?>">
			<span class="caption">Caption</span>
		</div>
	</div>
	<div class="row section">
		<h4 class="title-line t-center">Honda</h4>
	</div>
	<div class="row offers">
		<div class="col product">
			<a href="<?=base_url('index.php/main/detail')?>">
				<img src="<?=base_url('asset/images/cbr250rr.jpg')?>">
				<span class="caption">Caption</span>
			</a>
		</div>
		<div class="col product">
			<img src="<?=base_url('asset/images/cbr250rr.jpg')?>">
			<span class="caption">Caption</span>
		</div>
		<div class="col product">
			<img src="<?=base_url('asset/images/cbr250rr.jpg')?>">
			<span class="caption">Caption</span>
		</div>
		<div class="col product">
			<img src="<?=base_url('asset/images/cbr250rr.jpg')?>">
			<span class="caption">Caption</span>
		</div>
	</div>
	<div class="row section">
		<h4 class="title-line t-right bold">Kawasaki</h4>
	</div>
	<div class="row offers">
		<div class="col product">
			<img src="<?=base_url('asset/images/ninja.jpg')?>">
			<span class="caption">Caption</span>
		</div>
		<div class="col product">
			<img src="<?=base_url('asset/images/ninja.jpg')?>">
			<span class="caption">Caption</span>
		</div>
		<div class="col product">
			<img src="<?=base_url('asset/images/ninja.jpg')?>">
			<span class="caption">Caption</span>
		</div>
		<div class="col product">
			<img src="<?=base_url('asset/images/ninja.jpg')?>">
			<span class="caption">Caption</span>
		</div>
		<div class="col product">
			<img src="<?=base_url('asset/images/ninja.jpg')?>">
			<span class="caption">Caption</span>
		</div>
	</div>
	<div class="row section">
		<h4 class="title-line bold">Suzuki</h4>
	</div>
	<div class="row offers">
		<div class="col product loved">
			<img src="<?=base_url('asset/images/gsx1000rr.jpg')?>">
			<span class="caption">Caption</span>
		</div>
		<div class="col product">
			<img src="<?=base_url('asset/images/gsx1000rr.jpg')?>">
			<span class="caption">Caption</span>
		</div>
		<div class="col product">
			<img src="<?=base_url('asset/images/gsx1000rr.jpg')?>">
			<span class="caption">Caption</span>
		</div>
		<div class="col product">
			<img src="<?=base_url('asset/images/gsx1000rr.jpg')?>">
			<span class="caption">Caption</span>
		</div>
		<div class="col product">
			<img src="<?=base_url('asset/images/gsx1000rr.jpg')?>">
			<span class="caption">Caption</span>
		</div>
		<div class="col product">
			<img src="<?=base_url('asset/images/gsx1000rr.jpg')?>">
			<span class="caption">Caption</span>
		</div>
	</div>
</div>
<div class="clearfix"></div>
