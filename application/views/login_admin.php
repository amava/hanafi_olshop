<!DOCTYPE html>
<html>
<head>
	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
        <link rel="stylesheet" type="text/css" href="<?=base_url('asset/v4/css/bootstrap.min.css')?>">
        <link rel="stylesheet" type="text/css" href="<?=base_url('asset/plugins/fontawesome/css/font-awesome.min.css')?>">
        <link rel="stylesheet" type="text/css" href="<?=base_url('asset/css/style.css')?>">
        <script type="text/javascript" src="<?=base_url('asset/plugins/jquery/jquery.js')?>"></script>
        <style type="text/css">
        	.login-box{
        		background: linear-gradient(
	            rgba(255,255,255,0.7), 
	            rgba(255,255,255,0));
	            border-radius: 10px;
        	}
        	body{
        		background: url('../../asset/images/bg-login.jpg');
        		background-repeat: no-repeat;
        		background-attachment: fixed;
        		background-clip: border-box;
        		background-size: cover;
        	}
        </style>
    </head>
</head>
<body>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-4 px-5 py-5 login-box">
				<div class="row">
					<div class="col">
						<img src="<?=base_url('asset/images/logo.png')?>" style="max-width: 100%">
					</div>
				</div>
				<div class="row">
					<div class="col">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Username">							
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<div class="form-group">
							<input type="password" class="form-control" placeholder="Password">							
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<div class="form-group">
							<input type="submit" class="btn btn-block btn-primary" value="Login">							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>