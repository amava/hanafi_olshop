<div class="container-fluid admin">
	<div class="row">
		<div class="col-2">
			<aside class="sidebar">
				<ul class="sidebar-menu">
					<li><a href="<?=base_url('index.php/main/admin')?>"><i class="fa fa-shopping-basket" aria-hidden="true"></i> Produk</a></li>
					<li class="active"><a href="<?=base_url('index.php/main/banner')?>"><i class="fa fa-code" aria-hidden="true"></i> Iklan</a></li>
					<li><a href="<?=base_url('index.php/main/transaksi')?>"><i class="fa fa-exchange" aria-hidden="true"></i> Transaksi</a></li>
					<li><a href="<?=base_url('index.php/main/status_pengiriman')?>"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Status Pengiriman</a></li>
				</ul>
			</aside>
		</div>
		<div class="col mt-2">
			<div class="row">
				<div class="col-2 tab-item">Banner</div>
				<div class="col-2 tab-item">Promo</div>
				<div class="col-2 tab-item active">Diskon</div>
			</div>
			<div class="row">
				<div class="col-11 bg-grey py-2">
					<div class="row">
						<div class="col">
							<table class="table table-bordered table-striped" id="list">
								<thead>
									<tr>
										<th>Nama Barang</th>
										<th>Harga</th>
										<th>Pilih</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Celana Batik</td>
										<td>Rp 190.000,-</td>
										<td><input type="checkbox" name=""></td>
									</tr>
									<tr>
										<td>Celana Batik 2</td>
										<td>Rp 200.000,-</td>
										<td><input type="checkbox" name=""></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row float-right">
						<div class="col">
							<button class="btn btn-danger">Pilih</button>
						</div>
					</div>
					<div class="row mt-5">
						<div class="col">
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Barang</th>
										<th>Diskon</th>
										<th>Tanggal Berakhir diskon</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>Celana Batik</td>
										<td><input type="number" name="" class="form-control" style="width:80px" placeholder="%"></td>
										<td>31/12/2017</td>
									</tr>
									<tr>
										<td>2</td>
										<td>Celana Batik 2</td>
										<td><input type="number" name="" class="form-control" style="width:80px" placeholder="%"></td>
										<td>31/12/2017</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row float-right">
						<div class="col">
							<button class="btn btn-danger">Diskon</button>
						</div>
					</div>
					<div class="row mt-5">
						<div class="col">
							<table class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Barang</th>
										<th>Harga Normal</th>
										<th>Harga Diskon</th>
										<th>Tanggal Berakhir diskon</th>
										<th>Aksi</th>
										<th>Admin</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>Celana Batik Bunga</td>
										<td>Rp 200.000,-</td>
										<td>Rp 180.000,-</td>
										<td>31/12/2017</td>
										<td><i class="fa fa-times"></i></td>
										<td>Aat</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#list').DataTable({
			'ordering':false,
			'paging':false,
			'info':false
		});
	});
</script>