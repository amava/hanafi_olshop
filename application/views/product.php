<div class="container bg-grey">
	<div class="row px-1">
		<div class="col-3 filter-container">
			<div class="row pt-1">
				<div class="col-10">
					<h4 class="title-line grey">Pencarian Terkait</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-10">
					<p>Berdasarkan Harga</p>
				</div>
			</div>
			<div class="row">
				<div class="col-10">
					<div class="input-group">
						<span class="input-group-addon">Rp</span>
						<input type="text" name="" class="form-control" placeholder="Dari">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-10">
					<div class="input-group">
						<span class="input-group-addon">Rp</span>
						<input type="text" name="" class="form-control" placeholder="Sampai">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-10">
					<p class="text-addon">* Mulai dari harga terendah hingga tertinggi</p>
				</div>
			</div>
			<div class="row">
				<div class="col-10">
					<input type="submit" name="" class="btn btn-block btn-success" value="Submit">
				</div>
			</div>
		</div>
		<div class="col-9 py-2 products-container">
			<div class="row my-2">
				<div class="col product best mx-2">
					<img src="<?=base_url('asset/images/r1m.jpg')?>">
					<span class="info">
						<p class="nama">Nama Barang</p>
						<p class="harga">Rp 200.000 </p>
					</span>
				</div>
				<div class="col product mx-2">
					<img src="<?=base_url('asset/images/r1m.jpg')?>">
					<span class="info">
						<p class="nama">Nama Barang</p>
						<p class="harga">Rp 200.000 </p>
						<span class="discount">
							<p>Diskon</p>
							<p>15%</p>
						</span>
					</span>
				</div>
				<div class="col product mx-2">
					<img src="<?=base_url('asset/images/r1m.jpg')?>">
					<span class="info">
						<p class="nama">Nama Barang</p>
						<p class="harga">Rp 200.000 </p>
					</span>
				</div>
			</div>
			<div class="row my-2">
				<div class="col product mx-2">
					<img src="<?=base_url('asset/images/r1m.jpg')?>">
					<span class="info">
						<p class="nama">Nama Barang</p>
						<p class="harga">Rp 200.000 </p>
					</span>
				</div>
				<div class="col product mx-2">
					<img src="<?=base_url('asset/images/r1m.jpg')?>">
					<span class="info">
						<p class="nama">Nama Barang</p>
						<p class="harga">Rp 200.000 </p>
					</span>
				</div>
				<div class="col product mx-2">
					<img src="<?=base_url('asset/images/r1m.jpg')?>">
					<span class="info">
						<p class="nama">Nama Barang</p>
						<p class="harga">Rp 200.000 </p>
					</span>
				</div>
			</div>
			<nav aria-label="Page navigation example">
				<ul class="pagination justify-content-center mt-3">
				    <li class="page-item disabled">
				      <a class="page-link" href="#" aria-label="Previous">
				        <span aria-hidden="true">&laquo;</span>
				        <span class="sr-only">Previous</span>
				      </a>
				    </li>
				    <li class="page-item active"><a class="page-link" href="#">1</a></li>
				    <li class="page-item"><a class="page-link" href="#">2</a></li>
				    <li class="page-item"><a class="page-link" href="#">3</a></li>
				    <li class="page-item">
				      <a class="page-link" href="#" aria-label="Next">
				        <span aria-hidden="true">&raquo;</span>
				        <span class="sr-only">Next</span>
				      </a>
				    </li>
				</ul>
			</nav>
		</div>
	</div>
</div>