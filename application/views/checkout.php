<div class="container">
	<div class="row">
		<div class="col bg-grey" style="padding:20px">
			<div class="row title-holder title-line">
				<h4 class="red-color"><i class="fa fa-shopping-cart"></i> Keranjang Belanja</h4>
			</div>
			<div class="row">
				<div class="col table-holder">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th>Barang</th>
								<th class="red-color">Jenis</th>
								<th>Diskon</th>
								<th class="red-color">Harga</th>
								<th>Jumlah</th>
								<th class="red-color">Sub Total</th>	
							</tr>
						</thead>
						<tbody align="center">
							<tr class="item">
								<td><img src="<?=base_url('asset/images/r1m.jpg')?>" width=100></td>
								<td>Nama Barang</td>
								<td>jenis</td>
								<td>15%</td>
								<td>Rp 200.000</td>
								<td><input type="number" style="width: 50px;text-align: center" value="1"></td>
								<td>Rp 170.000</td>
							</tr>
							<tr class="item">
								<td><img src="<?=base_url('asset/images/cbr250rr.jpg')?>" width=100></td>
								<td>Nama Barang</td>
								<td>jenis</td>
								<td>15%</td>
								<td>Rp 200.000</td>
								<td><input type="number" style="width: 50px;text-align: center" value="1"></td>
								<td>Rp 170.000</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="row title-holder title-line">
				<h4 class="red-color"><i class="fa fa-shopping-cart"></i> Ekspedisi</h4>
			</div>
			<div class="row">
				<div class="col-7">
					<div class="row">
						<div class="col-3">
							<select class="form-control">
								<option>JNE</option>
								<option>TIKI</option>
								<option>J&T</option>
							</select>
						</div>
						<div class="col-3">
							<select class="form-control">
								<option>YES</option>
								<option>Reguler</option>
								<option>Ekonomis</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-3">
							<select class="form-control">
								<option>Provinsi</option>
								<option>Provinsi</option>
								<option>Provinsi</option>
								<option>Provinsi</option>
							</select>
						</div>
						<div class="col-3">
							<select class="form-control">
								<option>Kota/Kab</option>
								<option>Kota/Kab</option>
								<option>Kota/Kab</option>
								<option>Kota/Kab</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="row">
						<div class="col-5">
							<span class="valign-c">Ongkir</span>
						</div>
						<div class="col">
							<input type="text" class="form-control" value="Rp 20.000">
						</div>
					</div>
					<div class="row">
						<div class="col-5">
							<span class="valign-c">Total Produk</span>
						</div>
						<div class="col">
							<input type="text" class="form-control" value="Rp 340.000">
						</div>
					</div>
					<div class="row my-2">
						<div class="col-5">
							<span class="valign-c">Dibayar</span>
						</div>
						<div class="col total">
							<input type="text" class="form-control" value="Rp 360.000">
						</div>
					</div>
				</div>
			</div>
			<div class="row float-right">
				<div class="col">
					<button class="btn btn-danger">Lanjut</button>
				</div>
			</div>
		</div>
	</div>
</div>