<div class="container-fluid admin">
	<div class="row">
		<div class="col-2">
			<aside class="sidebar">
				<ul class="sidebar-menu">
					<li class="active"><a href="<?=base_url('index.php/main/admin')?>"><i class="fa fa-shopping-basket" aria-hidden="true"></i> Produk</a></li>
					<li><a href="<?=base_url('index.php/main/banner')?>"><i class="fa fa-code" aria-hidden="true"></i> Iklan</a></li>
					<li><a href="<?=base_url('index.php/main/transaksi')?>"><i class="fa fa-exchange" aria-hidden="true"></i> Transaksi</a></li>
					<li><a href="<?=base_url('index.php/main/status_pengiriman')?>"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Status Pengiriman</a></li>
				</ul>
			</aside>
		</div>
		<div class="col mt-2">
			<div class="row">
				<div class="col-2 tab-item">
					Tambah Produk
				</div>
				<div class="col-2 tab-item">
					Kategori
				</div>
				<div class="col-2 tab-item active">
					Daftar Produk
				</div>
			</div>
			<div class="row">
				<div class="col-12 bg-grey pt-3">
					<table class="table table-bordered table-striped" id="list" cellspacing="0" width="100%">
						<thead>
							<th>ID</th>
							<th>Nama Produk</th>
							<th>Tgl.Masuk</th>
							<th>Kategori</th>
							<th>Harga</th>
							<th>Berat</th>
							<th>Ukuran</th>
							<th>Detail</th>
							<th>Foto</th>
							<th>Ket</th>
							<th>Admin</th>
							<th>History</th>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Nama Produk</td>
								<td>31 Juli 2019</td>
								<td>Fashion Pria</td>
								<td>Rp. 200.000,- </td>
								<td>400gr</td>
								<td>M | L | XL</td>
								<td>detail produk</td>
								<td> foto </td>
								<td> <i class="fa fa-edit"></i> | <i class="fa fa-times"></i></td>
								<td>superadmin</td>
								<td></td>
							</tr>
							<tr>
								<td>2</td>
								<td>Nama Produk</td>
								<td>31 Juli 2019</td>
								<td>Fashion Pria</td>
								<td>Rp. 210.000,- </td>
								<td>400gr</td>
								<td>M | L | XL</td>
								<td>detail produk</td>
								<td> foto </td>
								<td> <i class="fa fa-edit"></i> | <i class="fa fa-times"></i></td>
								<td>superadmin</td>
								<td></td>
							</tr>
							<tr>
								<td>3</td>
								<td>Nama Produk</td>
								<td>31 Juli 2019</td>
								<td>Fashion Pria</td>
								<td>Rp. 400.000,- </td>
								<td>400gr</td>
								<td>M | L | XL</td>
								<td>detail produk</td>
								<td> foto </td>
								<td> <i class="fa fa-edit"></i> | <i class="fa fa-times"></i></td>
								<td>superadmin</td>
								<td></td>
							</tr>
							<tr>
								<td>4</td>
								<td>Nama Produk</td>
								<td>31 Juli 2019</td>
								<td>Fashion Pria</td>
								<td>Rp. 200.000,- </td>
								<td>400gr</td>
								<td>M | L | XL</td>
								<td>detail produk</td>
								<td> foto </td>
								<td> <i class="fa fa-edit"></i> | <i class="fa fa-times"></i></td>
								<td>superadmin</td>
								<td></td>
							</tr>
							<tr>
								<td>5</td>
								<td>Nama Produk</td>
								<td>31 Juli 2019</td>
								<td>Fashion Pria</td>
								<td>Rp. 200.000,- </td>
								<td>400gr</td>
								<td>M | L | XL</td>
								<td>detail produk</td>
								<td> foto </td>
								<td> <i class="fa fa-edit"></i> | <i class="fa fa-times"></i></td>
								<td>superadmin</td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
	    $('#list').DataTable({
	    	"paging":   false,
	        "ordering": false,
	        "info":     false
	    });
	});
</script>