<div class="container">
	<div class="row">
		<div class="col bg-grey" style="padding:20px">
			<div class="row title-holder title-line">
				<h4 class="red-color"><i class="fa fa-shopping-cart"></i> Data Penerima</h4>
			</div>
			<div class="row">
				<div class="col">
					<div class="row no-gutters">
						<h5 class="red-color">Silahkan isi data penerima barang sesuai dengan identitas.</h5>
					</div>
					<div class="row">
						<div class="col-2">
							<span class="valign-c">Nama Penerima <italic class="red-color">*</italic></span>
						</div>
						<div class="col-4">
							<input type="text" class="form-control" name="">
						</div>
					</div>
					<div class="row">
						<div class="col-2">
							<span class="valign-c">Alamat Penerima <italic class="red-color">*</italic></span>
						</div>
						<div class="col-4">
							<textarea class="form-control"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col-2">
							<span class="valign-c">No Telepon / HP <italic class="red-color">*</italic></span>
						</div>
						<div class="col-3">
							<input type="text" class="form-control" name="">
						</div>
					</div>
					<div class="row">
						<div class="col-2">
							<span class="valign-c">Kode Pos <italic class="red-color">*</italic></span>
						</div>
						<div class="col-2">
							<input type="text" class="form-control" name="">
						</div>
					</div>
					<div class="row">
						<div class="col-2">
							<span class="valign-c">Kota <italic class="red-color">*</italic></span>
						</div>
						<div class="col-3">
							<input type="text" class="form-control" name="">
						</div>
					</div>
				</div>
			</div>
			<div class="row float-right">
				<div class="col">
					<button class="btn btn-danger">Lanjut</button>
				</div>
			</div>
		</div>
	</div>
</div>