<div class="container-fluid admin">
	<div class="row">
		<div class="col-2">
			<aside class="sidebar">
				<ul class="sidebar-menu">
					<li><a href="<?=base_url('index.php/main/admin')?>"><i class="fa fa-shopping-basket" aria-hidden="true"></i> Produk</a></li>
					<li class="active"><a href="<?=base_url('index.php/main/banner')?>"><i class="fa fa-code" aria-hidden="true"></i> Iklan</a></li>
					<li><a href="<?=base_url('index.php/main/transaksi')?>"><i class="fa fa-exchange" aria-hidden="true"></i> Transaksi</a></li>
					<li><a href="<?=base_url('index.php/main/status_pengiriman')?>"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Status Pengiriman</a></li>
				</ul>
			</aside>
		</div>
		<div class="col mt-2">
			<div class="row">
				<div class="col-2 tab-item">Banner</div>
				<div class="col-2 tab-item active">Promo</div>
				<div class="col-2 tab-item">Diskon</div>
			</div>
			<div class="row">
				<div class="col-11 bg-grey pb-2">
					<div class="row pt-5 align-items-center">
						<div class="col-6">
							<label class="upload-banner"><i class="fa fa-3x fa-camera valign-c"></i></label>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<p class="text-addon red-color">*Rekomendasi Ukuran Promo 1000x700, maksimal 1Mb.</p>
						</div>
					</div>
					<div class="row">
						<div class="col-8">
							<textarea class="form-control" rows="5"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<p class="text-addon red-color">*Deskripsi promo untuk tabel admin.</p>
						</div>
					</div>
					<div class="row float-right">
						<div class="col">
							<button class="btn btn-danger">Terbitkan</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>