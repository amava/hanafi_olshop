<div class="container-fluid admin">
	<div class="row">
		<div class="col-2">
			<aside class="sidebar">
				<ul class="sidebar-menu">
					<li><a href="<?=base_url('index.php/main/admin')?>"><i class="fa fa-shopping-basket" aria-hidden="true"></i> Produk</a></li>
					<li class="active"><a href="<?=base_url('index.php/main/banner')?>"><i class="fa fa-code" aria-hidden="true"></i> Iklan</a></li>
					<li><a href="<?=base_url('index.php/main/transaksi')?>"><i class="fa fa-exchange" aria-hidden="true"></i> Transaksi</a></li>
					<li><a href="<?=base_url('index.php/main/status_pengiriman')?>"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Status Pengiriman</a></li>
				</ul>
			</aside>
		</div>
		<div class="col mt-2">
			<div class="row">
				<div class="col-2 tab-item active">Transaksi</div>
			</div>
			<div class="row">
				<div class="col-11 bg-grey py-2">
					<div class="row">
						<div class="col">
							<table class="table table-bordered table-striped" id="list">
								<thead>
									<tr>
										<th>ID</th>
										<th>Nama Pembeli</th>
										<th>Tanggal</th>
										<th>Bukti Transaksi</th>
										<th>Ket</th>
										<th>Konfirmasi</th>
										<th>Admin</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>Adit</td>
										<td>21 Januari 2019</td>
										<td>IMG</td>
										<td><span class="badge badge-primary">Open Detail</span></td>
										<td>Konfirmasi</td>
										<td>Aat</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Adit</td>
										<td>21 Januari 2019</td>
										<td>IMG</td>
										<td><span class="badge badge-primary">Open Detail</span></td>
										<td>Konfirmasi</td>
										<td>Aat</td>
									</tr>
									<tr>
										<td>1</td>
										<td>Adit</td>
										<td>21 Januari 2019</td>
										<td>IMG</td>
										<td><span class="badge badge-primary">Open Detail</span></td>
										<td>Konfirmasi</td>
										<td>Aat</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col pl-5">
							<p class="text-addonn red-color bold">Mepi</p>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<table class="table table-striped table-bordered">
								<thead >
									<tr>
										<th class="t-valign" rowspan="2">Produk</th>
										<th class="t-valign" rowspan="2">Jumlah</th>
										<th class="t-valign" colspan="4">Produk dibeli</th>
										<th class="t-valign" colspan="2">Ekspedisi</th>
										<th class="t-valign" rowspan="2">Total</th>
										<th class="t-valign" rowspan="2">Alamat Penerima</th>
									</tr>
									<tr>
										<th>Harga/item</th>
										<th>Diskon</th>
										<th>Promo</th>
										<th>Total Produk</th>
										<th>Nama</th>
										<th>Tarif</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Celana Batik Tulis</td>
										<td>2</td>
										<td>Rp. 120.000,-</td>
										<td>5%</td>
										<td>-</td>
										<td>Tp. 228.000,-</td>
										<td>JNE YES</td>
										<td>Rp. 32.000,-</td>
										<td>Rp. 260.000,-</td>
										<td>Jl konoha barat</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#list').DataTable({
			"paging":   false,
	        "ordering": false,
	        "info":     false
		});
	});
</script>