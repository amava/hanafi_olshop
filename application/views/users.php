<div class="container-fluid admin">
	<div class="row">
		<div class="col-2">
			<aside class="sidebar">
				<ul class="sidebar-menu">
					<li class="active"><a href="<?=base_url('index.php/main/users')?>"><i class="fa fa-users" aria-hidden="true"></i> Users</a></li>
					<li><a href="<?=base_url('index.php/main/admin')?>"><i class="fa fa-shopping-basket" aria-hidden="true"></i> Produk</a></li>
					<li><a href="<?=base_url('index.php/main/banner')?>"><i class="fa fa-code" aria-hidden="true"></i> Iklan</a></li>
					<li><a href="<?=base_url('index.php/main/transaksi')?>"><i class="fa fa-exchange" aria-hidden="true"></i> Transaksi</a></li>
					<li><a href="<?=base_url('index.php/main/status_pengiriman')?>"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Status Pengiriman</a></li>
				</ul>
			</aside>
		</div>
		<div class="col mt-2">
			<div class="row">
				<div class="col-2 tab-item active">List User</div>
				<div class="col-2 tab-item">Tambah User</div>
			</div>
			<div class="row">
				<div class="col-11 bg-grey py-2">
					<table class="table table-bordered table-stripped" id="list">
						<thead>
							<tr>
								<th>No</th>
								<th>Username</th>
								<th>Nama</th>
								<th>Hak Akses</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>1</th>
								<th>Superadmin</th>
								<th>SUperadmin</th>
								<th>Superadmin</th>
								<th><label class="btn btn-info"><i class="fa fa-edit"></i></label>&nbsp;<label class="btn btn-danger"><i class="fa fa-remove"></i></label> </th>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#list').DataTable({
			'ordering':false,
			'paging':false,
			'info':false
		});
	});
</script>