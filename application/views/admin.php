<div class="container-fluid admin">
	<div class="row">
		<div class="col-2">
			<aside class="sidebar">
				<ul class="sidebar-menu">
					<li class="active"><a href="<?=base_url('index.php/main/admin')?>"><i class="fa fa-shopping-basket" aria-hidden="true"></i> Produk</a></li>
					<li><a href="<?=base_url('index.php/main/banner')?>"><i class="fa fa-code" aria-hidden="true"></i> Iklan</a></li>
					<li><a href="<?=base_url('index.php/main/transaksi')?>"><i class="fa fa-exchange" aria-hidden="true"></i> Transaksi</a></li>
					<li><a href="<?=base_url('index.php/main/status_pengiriman')?>"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Status Pengiriman</a></li>
				</ul>
			</aside>
		</div>
		<div class="col mt-2">
			<div class="row">
				<div class="col-2 tab-item active">
					Tambah Produk
				</div>
				<div class="col-2 tab-item">
					Kategori
				</div>
				<div class="col-2 tab-item">
					Daftar Produk
				</div>
			</div>
			<div class="row">
				<div class="col-11 bg-grey">
					<div class="alert alert-success alert-dismissible fade show mt-2" role="alert">
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
					  <strong><i class="fa fa-check"></i></strong> Produk berhasil ditambah
					</div>
					<form class="mt-3">
						<div class="row form-group">
							<div class="col-2">ID</div>
							<div class="col-4">19</div>
						</div>
						<div class="row no-gutters form-group">
							<div class="col-2">
								<span class="valign-c">Nama Produk</span>
							</div>
							<div class="col-3">
								<input type="text" class="form-control" name="">
							</div>
						</div>
						<div class="row no-gutters form-group">
							<div class="col-2">
								<span class="valign-c">Tanggal Masuk</span>
							</div>
							<div class="col-3 input-group">							
								<input type="text" class="form-control" name="" data-toggle='datepicker'>
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
						<div class="row no-gutters form-group">
							<div class="col-2">
								<span class="valign-c">Kategori</span>
							</div>
							<div class="col-3">
								<select class="form-control">
									<optgroup label="Fashion">
										<option>opt 1</option>
										<option>opt 2</option>
										<option>opt 3</option>
									</optgroup>
									<optgroup label="Souvenir">
										<option>opt 1</option>
										<option>opt 2</option>
										<option>opt 3</option>
									</optgroup>
								</select>
							</div>
						</div>
						<div class="row no-gutters form-group">
							<div class="col-2">
								<span class="valign-c">Harga</span>
							</div>
							<div class="col-3">
								<input type="text" class="form-control" name="">
							</div>
						</div>
						<div class="row no-gutters form-group">
							<div class="col-2">
								<span class="valign-c">Stok</span>
							</div>
							<div class="col-3">
								<input type="number" class="form-control" name="">
							</div>
						</div>
						<div class="row no-gutters form-group">
							<div class="col-2">
								<span class="valign-c">Ukuran</span>
							</div>
							<div class="col-3">
								<input type="text" class="form-control" name="">
							</div>
							<div class="col-2 pl-5">
								<span class="valign-c">Warna</span>
							</div>
							<div class="col-3">
								<input type="text" class="form-control" name="">
							</div>
						</div>
						<div class="row no-gutters form-group">
							<div class="col-2">
								<span class="valign-c">Detail</span>
							</div>
							<div class="col-8">
								<textarea rows="5" class="form-control"></textarea>
							</div>
						</div>
						<div class="row no-gutters form-group">
							<div class="col-2">
								<span class="valign-c">Foto</span>
							</div>
							<div class="col-1">
								<input id="img" type="file" class="form-control-file" name="" style="display: none">
								<label for="img" class="upload-img"><i class="fa fa-plus"></i></label>
							</div>
						</div>
						<div class="row no-gutters form-group">
							<div class="col-2"></div>
							<div class="col-2">
								<input type="submit" value="Simpan" class="btn btn-block btn-success" name="">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>