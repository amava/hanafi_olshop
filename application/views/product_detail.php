<div class="container py-2 px-2 bg-grey">
	<div class="row">
		<div class="col-7">
			<div class="row">
				<div class="col-9 best" id="product_detail">
					<div class="glide" id="Glide">
					    <div class="glide__wrapper">
					        <ul class="glide__track">
					            <li class="glide__slide"><img src="<?=base_url('asset/images/cbr250rr.jpg')?>"></li>
					            <li class="glide__slide"><img src="<?=base_url('asset/images/r1m.jpg')?>"></li>
					            <li class="glide__slide"><img src="<?=base_url('asset/images/ninja.jpg')?>"></li>
					        </ul>
					    </div>
					    <div class="glide__bullets"></div>
					</div>
				</div>
				<div class="col">
					<ul class="small-img">
						<li>
							<a href="#" data-glide-trigger='#Glide' data-glide-dir='=1'>
								<img src="<?=base_url('asset/images/cbr250rr.jpg')?>" style="max-width: 100%">
							</a>
						</li>
						<li>
							<a href="#" data-glide-trigger='#Glide' data-glide-dir='=2'>
								<img src="<?=base_url('asset/images/r1m.jpg')?>" style="max-width: 100%">
							</a>
						</li>
						<li>
							<a href="#" data-glide-trigger='#Glide' data-glide-dir='=3'>
								<img src="<?=base_url('asset/images/ninja.jpg')?>" style="max-width: 100%">
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col">
			<div class="row title-line">
				<h4>Nama Barang</h4>
			</div>
			<div class="row holder-white-r no-gutters py-2 mt-3">
				<div class="col-3 t-right">
					<span class="valign-c">Harga</span>
				</div>
				<div class="col t-center">
					<span class="harga large">Rp 200.000.000,-</span>
				</div>
			</div>
			<div class="row mt-2">
				<h4 class="bold">Detail</h4>
			</div>
			<div class="row no-gutters">
				<div class="col-3">
					Ukuran
				</div>
				<div class="col pl-4 holder-white">
					S M L
				</div>
			</div>
			<div class="row no-gutters">
				<div class="col-3">
					Warna
				</div>
				<div class="col pl-4 holder-white">
					Biru
				</div>
			</div>
			<div class="row no-gutters">
				<div class="col-3">
					Keterangan
				</div>
				<div class="col pl-4 holder-white">
					Isi aja keterangan disini
				</div>
			</div>
			<div class="row mt-2 no-gutters">
				<div class="col-3">
					<span class="valign-c">Jumlah</span>
				</div>
				<div class="col-3">
					<input type="number" name="" class="form-control">
				</div>
			</div>
		</div>
	</div>
	<div class="row ">
		<div class="col-10"></div>
		<div class="col">
			<button class="btn btn-danger"> Masuk Keranjang </button>		
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.glide').glide({
			type: "carousel"
		});
	});
</script>