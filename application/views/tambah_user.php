<div class="container-fluid admin">
	<div class="row">
		<div class="col-2">
			<aside class="sidebar">
				<ul class="sidebar-menu">
					<li class="active"><a href="<?=base_url('index.php/main/users')?>"><i class="fa fa-users" aria-hidden="true"></i> Users</a></li>
					<li><a href="<?=base_url('index.php/main/admin')?>"><i class="fa fa-shopping-basket" aria-hidden="true"></i> Produk</a></li>
					<li><a href="<?=base_url('index.php/main/banner')?>"><i class="fa fa-code" aria-hidden="true"></i> Iklan</a></li>
					<li><a href="<?=base_url('index.php/main/transaksi')?>"><i class="fa fa-exchange" aria-hidden="true"></i> Transaksi</a></li>
					<li><a href="<?=base_url('index.php/main/status_pengiriman')?>"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Status Pengiriman</a></li>
				</ul>
			</aside>
		</div>
		<div class="col mt-2">
			<div class="row">
				<div class="col-2 tab-item">List User</div>
				<div class="col-2 tab-item active">Tambah User</div>
			</div>
			<div class="row">
				<div class="col-11 bg-grey py-2">
					<div class="alert alert-success alert-dismissible fade show mt-2" role="alert">
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
					  <strong><i class="fa fa-check"></i></strong> User berhasil ditambah
					</div>
					<form class="mt-3">
						<div class="row no-gutters form-group">
							<div class="col-2">
								<span class="valign-c">Username</span>
							</div>
							<div class="col-3">
								<input type="text" class="form-control" name="">
							</div>
						</div>
						<div class="row no-gutters form-group">
							<div class="col-2">
								<span class="valign-c">Nama Lengkap</span>
							</div>
							<div class="col-3">
								<input type="text" class="form-control" name="">
							</div>
						</div>
						<div class="row no-gutters form-group">
							<div class="col-2">
								<span class="valign-c">Hak Akses</span>
							</div>
							<div class="col-3">
								<select class="form-control">
									<option>Admin</option>
									<option>SuperAdmin</option>
									<option>User</option>
								</select>
							</div>
						</div>
						<div class="row no-gutters form-group">
							<div class="col-2">
								<span class="valign-c">Password</span>
							</div>
							<div class="col-3">
								<input type="password" class="form-control" name="">
							</div>
						</div>
						<div class="row no-gutters form-group">
							<div class="col-2">
								<span class="valign-c">Konfirmasi Password</span>
							</div>
							<div class="col-3">
								<input type="password" class="form-control" name="">
							</div>
						</div>
						<div class="row no-gutters form-group">
							<div class="col-2"></div>
							<div class="col-2">
								<input type="submit" value="Simpan" class="btn btn-block btn-success" name="">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>