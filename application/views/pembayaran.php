<div class="container">
	<div class="row">
		<div class="col bg-grey" style="padding:20px">
			<div class="row title-holder title-line">
				<h4 class="red-color"><i class="fa fa-shopping-cart"></i> Pembayaran</h4>
			</div>
			<div class="row">
				<div class="col-5 t-center">
					<label class="fa-stack fa-5x" for="bukti">
					  <i class="fa fa-square-o fa-stack-2x"></i>
					  <i class="fa fa-camera fa-stack-1x"></i>
					</label>
					<input type="file" style="display: none" name="bukti" id="bukti">
				</div>
				<div class="col-6 red-color">
					<div class="row">
						<div class="col">
							<p>Silahkan upload struk / bukti pembayaran kamu untuk mengkonfirmasi bahwa kamu telah menyelesaikan pembayaran untuk produk yang dibeli.</p>
						</div>
					</div>
					<div class="row">
						<div class="col rekening mx-1 t-center">
							<img src="<?=base_url('asset/images/bca.png')?>" width="125" height="57">
							<p>667829910-29093</p>
							<p>a/n PT. Kereta Api Indonesia</p>
						</div>
						<div class="col rekening mx-1 t-center">
							<img src="<?=base_url('asset/images/bri.png')?>" width="125" height="57">
							<p>667829910-29093</p>
							<p>a/n PT. Kereta Api Indonesia</p>
						</div>
						<div class="col rekening mx-1 t-center">
							<img src="<?=base_url('asset/images/mandiri.png')?>" width="125" height="57">
							<p>667829910-29093</p>
							<p>a/n PT. Kereta Api Indonesia</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row float-right">
				<div class="col">
					<button class="btn btn-danger">Lanjut</button>
				</div>
			</div>
		</div>
	</div>
</div>