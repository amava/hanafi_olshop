<!DOCTYPE html>
<HTML>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Online Shop</title>
        <link rel="stylesheet" type="text/css" href="<?=base_url('asset/v4/css/bootstrap.min.css')?>">
        <link rel="stylesheet" type="text/css" href="<?=base_url('asset/plugins/fontawesome/css/font-awesome.min.css')?>">
        <link rel="stylesheet" type="text/css" href="<?=base_url('asset/css/style.css')?>">
        <link rel="stylesheet" type="text/css" href="<?=base_url('asset/plugins/datatables/datatables.min.css')?>">
        <link rel="stylesheet" type="text/css" href="<?=base_url('asset/plugins/glide/css/glide.core.min.css')?>">
        <link rel="stylesheet" type="text/css" href="<?=base_url('asset/plugins/glide/css/glide.theme.min.css')?>">
        <link rel="stylesheet" type="text/css" href="<?=base_url('asset/plugins/datepicker/datepicker.css')?>">
        <script type="text/javascript" src="<?=base_url('asset/plugins/jquery/jquery.js')?>"></script>
    </head>
<body>
<div class="se-pre-con"></div>
<div class="header">
    <div class="row">
    <?php if(!isset($admin)){ ?>
        <div class="mini-holder col-12">
            <div class="item">
                <a href="#">Bantuan</a> | Ikuti Kami &nbsp; <a href="http://facebook.com"><i class="fa fa-lg fa-facebook-square"></i></a> &nbsp; <a href="http://instagram.com"><i class="fa fa-lg fa-instagram"></i></a>
            </div>
        </div>
    <?php } ?>
    </div>
    <div class="row main-holder">
        <div class="col-3 logo-holder">
            <img src="<?=base_url('asset/images/logo.png')?>">
        </div>
    <?php if(isset($admin)){ ?>
		<div class="col-6 category-holder">
            <div class="valign">Admin </div>
        </div>
    <?php }else{ ?>
        <div class="col-5 px-0">
            <div class="input-group search valign">
                <input type="text" name="search" class="form-control" placeholder="Cari Produk">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
            </div>
        </div>
        <div class="col category-holder t-right" id="category-holder">
            <div class="valign">Kategori <i class="fa fa-angle-down"></i> </div>
        </div>
    <?php } ?>
    <?php if(isset($_GET['auth'])||isset($auth)){ ?>
        <div class="col menu-holder">
            <div class="valign" >
                <span class="btn btn-outline-danger border-white"><i class="fa fa-shopping-cart text-white"></i></span>&nbsp;<span class="btn btn-outline-danger border-white" id="notif-button"><i class="fa fa-bell-o text-white"></i> <span class="badge badge-pill badge-success">2</span></span> &nbsp; <img class="profil-thumb" src="<?=base_url('asset/images/profil.jpg')?>">
            </div>
            <div class="notification-body closed">
                <div class="row no-gutters mt-2">
                    <ul class="menu-content">
                        <li><span class="badge badge-pill badge-success">1</span> Status Pemesanan</li>
                        <li><span class="badge badge-pill badge-success">1</span> Status Pengiriman</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="menu-profile closed px-3">
            <div class="row no-gutters pt-2" style="border-top:2px solid #fbe6e6">
                <img src="<?=base_url('asset/images/profil.jpg')?>" style="max-height: 160px;width: 100%;border-radius: 50%;">
            </div>
            <div class="row no-gutters mt-2">
                <ul class="menu-content">
                    <li>Ubah Profil</li>
                    <li>Pesanan Saya</li>
                    <li>Histori</li>
                    <li>Keluar</li>
                </ul>
            </div>
        </div>
    <?php }else{ ?>
        <div class="col menu-holder">
            <div class="valign">
                <i class="fa fa-shopping-cart"></i> | <span class="btnModal" data-toggle="modal" data-target="#modalLogin">Masuk</span> | <span class="btnModal" data-toggle="modal" data-target="#modalDaftar">Daftar</span>
            </div>
        </div>
    <?php } ?>
    <?php if(!isset($admin)){ ?>
        <div class="row" id="category-content">
            <div class="col px-1">
                <div class="row">
                    <div class="col-4 px-0">
                        <img class="col-12" src="<?=base_url('asset/images/shirt.png')?>">
                    </div>
                    <div class="col pl-2 pr-0">
                        <ul class="category-item">
                            <li class="title">Fashion</li>
                            <li><i class="fa fa-male"></i> <a href="#">Fashion Pria</a></li>
                            <li><i class="fa fa-female"></i> <a href="#">Fashion Wanita</a></li>
                            <li><i class="fa fa-sticky-note-o"></i> <a href="#">Kain Batik</a></li>
                        </ul>
                    </div>
                </div>      
            </div>
            <div class="col px-1">
                <div class="row">
                    <div class="col-4 px-0">
                        <img class="col-12" src="<?=base_url('asset/images/cake.png')?>">
                    </div>
                    <div class="col pl-2 pr-0">
                        <ul class="category-item">
                            <li class="title">Jajanan</li>
                            <li><i class="fa fa-cutlery"></i> <a href="#">Jajanan 1</a></li>
                            <li><i class="fa fa-cutlery"></i> <a href="#">Jajanan 2</a></li>
                            <li><i class="fa fa-cutlery"></i> <a href="#">Jajanan 3</a></li>
                        </ul>
                    </div>
                </div>      
            </div>
            <div class="col px-1">
                <div class="row">
                    <div class="col-4 px-0">
                        <img class="col-12" src="<?=base_url('asset/images/merch.png')?>">
                    </div>
                    <div class="col pl-2 pr-0">
                        <ul class="category-item">
                            <li class="title">Souvenir</li>
                            <li><i class="fa fa-tags"></i> <a href="#">Souvenir 1</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">Souvenir 2</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">Souvenir 3</a></li>
                        </ul>
                    </div>
                </div>      
            </div>
            <div class="col px-1">
                <div class="row">
                    <div class="col-4 px-0">
                        <img class="col-12" src="<?=base_url('asset/images/merch.png')?>">
                    </div>
                    <div class="col pl-2 pr-0">
                        <ul class="category-item">
                            <li class="title">Kategori 4</li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 1</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 2</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 3</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 4</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 5</a></li>
                        </ul>
                    </div>
                    <div class="col px-1">
                        <ul class="category-item">
                            <li>&nbsp;</li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 6</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 7</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 8</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 9</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 10</a></li>
                        </ul>
                    </div>
                </div>      
            </div>
            <div class="col px-1">
                <div class="row">
                    <div class="col-4 px-0">
                        <img class="col-12" src="<?=base_url('asset/images/merch.png')?>">
                    </div>
                    <div class="col pl-2 pr-0">
                        <ul class="category-item">
                            <li class="title">Kategori 4</li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 1</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 2</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 3</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 4</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 5</a></li>
                        </ul>
                    </div>
                    <div class="col px-1">
                        <ul class="category-item">
                            <li>&nbsp;</li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 6</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 7</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 8</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 9</a></li>
                            <li><i class="fa fa-tags"></i> <a href="#">item 10</a></li>
                        </ul>
                    </div>
                </div>      
            </div>
        </div>
    <?php } ?>
    </div>
</div>
<?php if(!isset($admin)){ ?>
<!-- Modal Login -->
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <span class="modal-close" data-dismiss="modal"><i class="fa fa-lg fa-times"></i></span>
        <div class="col-12">
            <div class="row">
                <img src="<?=base_url('asset/images/logo2.png')?>" style="margin: 0 auto;height: 130px;">
            </div>
            <div class="row">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                  <input type="email" class="form-control" placeholder="Email">
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-key"></i></span>
                  <input type="password" class="form-control" placeholder="Password">
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="#" class="label bold float-right">Lupa Password?</a>
                </div>
            </div>
            <br>
            <div class="row">
                <button class="btn btn-block btn-danger">Masuk</button>
            </div>
            <div class="row">
                <div class="col-12">
                    <span class="label">Belum punya akun ? </span><a href="#" class="label bold">Daftar</a>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal register -->
<div class="modal fade" id="modalDaftar" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <span class="modal-close" data-dismiss="modal"><i class="fa fa-lg fa-times"></i></span>
        <div class="col-12">
            <div class="row">
                <img src="<?=base_url('asset/images/logo2.png')?>" style="margin: 0 auto;height: 130px;">
            </div>
            <div class="row">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-id-card-o"></i></span>
                  <input type="text" class="form-control" placeholder="Nama Terang">
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-user-o"></i></span>
                  <input type="text" class="form-control" placeholder="Username">
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                  <input type="email" class="form-control" placeholder="Email">
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-key"></i></span>
                  <input type="password" class="form-control" placeholder="Password">
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="#" class="label bold float-right">Lupa Password?</a>
                </div>
            </div>
            <br>
            <div class="row">
                <button class="btn btn-block btn-danger">Daftar</button>
            </div>
            <div class="row">
                <div class="col-12">
                    <span class="label">Belum punya akun ? </span><a href="#" class="label bold">Daftar</a>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(window).scroll(function(){
            var current = $(window).scrollTop();
            if(current > 200 ){
                $('.header').css('top','-25px');
                $('.main-holder').css('height','40px');
                $('.menu-profile').css('padding-top','45px');
                $('.notification-body').css('margin-top','2px');
                $('#category-content').addClass('mini');
            }else{
                $('.header').css('top','0px');
                $('.main-holder').css('height','65px');
                $('.menu-profile').css('padding-top','70px');
                $('.notification-body').css('margin-top','25px');
                $('#category-content').removeClass('mini');
            }
        });

        $('.profil-thumb').click(function(){
            if($('.menu-profile').hasClass('closed')){
                if(!$('.notification-body').hasClass('closed')){
                    $('.notification-body').addClass('closed');
                    $('.menu-profile').removeClass('closed');
                }else{
                    $('.menu-profile').removeClass('closed');
                }
            }else{
                $('.menu-profile').addClass('closed');
            }
        });
        $('#notif-button').click(function(){
            if($('.notification-body').hasClass('closed')){
                if(!$('.menu-profile').hasClass('closed')){
                    $('.menu-profile').addClass('closed');
                }
                $('.notification-body').removeClass('closed');
            }else{
                $('.notification-body').addClass('closed');
            }
        });
    })
</script>
<?php } ?>
