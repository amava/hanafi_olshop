<footer class="footer">
	<div class="col">
		<div class="row">
			<div class="col">
				<h5 class="footer-title">Sitemap</h5>
				<ul class="footer-item">
					<li><i class="fa fa-question-circle"></i> Pusat Bantuan</li>
					<li><i class="fa fa-credit-card-alt"></i> Pembayaran</li>
					<li><i class="fa fa-shopping-cart"></i> Cara Pembelian</li>
					<li><i class="fa fa-paper-plane"></i> Pengiriman</li>
					<li><i class="fa fa-book"></i> Syarat & Ketentuan</li>
				</ul>
			</div>
			<div class="col">
				<div class="row">
					<h5 class="footer-title">Find Us : </h5>
				</div>
				<div class="row">
					<div class="col t-center">
						<span class="btn btn-outline-secondary"><i class="fa fa-facebook"></i></span>
					</div>
					<div class="col t-center">
						<span class="btn btn-outline-secondary"><i class="fa fa-instagram"></i></span>
					</div>
					<div class="col t-center">
						<span class="btn btn-outline-secondary"><i class="fa fa-twitter"></i></span>
					</div>
				</div>
				<div class="row">
					<div class="col"></div>
					<div class="col t-center mr-5">
						<span class="btn btn-outline-secondary"><i class="fa fa-youtube"></i></span>
					</div>
					<div class="col t-center">
						<span class="btn btn-outline-secondary"><i class="fa fa-pinterest"></i></span>
					</div>
					<div class="col"></div>
				</div>
			</div>
			<div class="col">
				<div class="row">
					<h5 class="footer-title">Langganan</h5>
				</div>
				<div class="row">
					<div class="col-8">
						<input type="text" class="form-control" placeholder="Email Address">	
					</div>
					<div class="col">
						<button class="btn btn-outline-primary">Subscribe</button>	
					</div>
				</div>
			</div>
		</div>
		<div class="row t-center">
			<span class="copyright">Copyright &#169; 2017 Aditya marselvada. All Rights Reserved. </span>
		</div>
	</div>
</footer>
<a href="#0" class="to-top"><i class="fa fa-arrow-up valign-c"></i></a>