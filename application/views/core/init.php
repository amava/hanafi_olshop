
<script type="text/javascript" src="<?=base_url('asset/v4/popper.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/v4/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/plugins/jquery/modernizr.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/plugins/glide/glide.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/plugins/datepicker/datepicker.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('asset/plugins/datatables/dataTables.bootstrap4.min.js')?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/scrollReveal.js/3.3.6/scrollreveal.min.js"></script>
<script type="text/javascript">
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});
	$(document).ready(function(){
		$('[data-toggle="datepicker"]').datepicker({
			autoHide:true,
		});
		
		// BACK TO TOP
	 	// browser window scroll (in pixels) after which the "back to top" link is shown
	    var offset = 300,
	        //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
	        offset_opacity = 1200,
	        //duration of the top scrolling animation (in ms)
	        scroll_top_duration = 900,
	        //grab the "back to top" link
	        $back_to_top = $('.to-top');

	    //hide or show the "back to top" link
	    $(window).scroll(function () {
	        ($(this).scrollTop() > offset) ? $back_to_top.fadeIn() : $back_to_top.fadeOut();
	        if ($(this).scrollTop() > offset_opacity) {
	            $back_to_top.addClass('cd-fade-out');
	        }
	    });

	    //smooth scroll to top
	    $back_to_top.on('click', function (event) {
	        event.preventDefault();
	        $('body,html').animate({
	            scrollTop: 0,
	        }, scroll_top_duration
	        );
	    });

	    $(".logo-holder").attr("onclick", "location.href='<?=base_url()?>'");
	});
</script>
</body>
</HTML>