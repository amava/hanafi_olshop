<div class="container">
	<div class="row">
		<div class="col bg-grey" style="padding:20px">
			<div class="row title-holder title-line">
				<h4 class="red-color"><i class="fa fa-shopping-cart"></i> Status Pembelian</h4>
			</div>
			<div class="row">
				<div class="col">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No Transaksi Pembelian</th>
								<th class="red-color">Tanggal</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody align="center">
							<tr>
								<td>220102</td>
								<td>27/09/2017</td>
								<td>Konfirmasi</td>
							</tr>
							<tr>
								<td>220103</td>
								<td>27/09/2017</td>
								<td>Belum Konfirmasi</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="row title-holder title-line">
				<h4 class="red-color"><i class="fa fa-shopping-cart"></i> Status Pengiriman</h4>
			</div>
			<div class="row">
				<div class="col">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No Transaksi Pembelian</th>
								<th class="red-color">Nomor Resi</th>
								<th>Tanggal Pengiriman</th>
							</tr>
						</thead>
						<tbody align="center">
							<tr>
								<td>220101</td>
								<td>ax101000202</td>
								<td>26/09/2017</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>